<%@ page import="java.sql.*"%>
<%@ page import="javax.naming.Context"%>
<%@ page import="javax.naming.InitialContext"%>
<%@ page import="javax.naming.NamingException"%>
<%@ page import="javax.sql.DataSource"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Demo JDBC/JNDI Database connection</title>
  </head>
  
  <body>
    <h1>App1 connecting with JNDI MySQL database</h1>
    <h2>Data from database</h2>
    <%
      try {
        // Declarations
        Context ctx = null;    
        DataSource ds = null;
        Connection connection = null;
        Statement statement = null;
        ResultSet rs = null;

        // Initialisations
        ctx = new InitialContext();
        ds = (DataSource) ctx.lookup("java:/comp/env/jdbc/JndiDBName");

        connection = ds.getConnection();
        statement = connection.createStatement();

        // Database querying; use of data
        String SQLQuery = "SELECT username, firstname, lastname from users";
        rs = statement.executeQuery(SQLQuery);

        while (rs.next()) {           
          String usn = rs.getString("username");
          String fstnm = rs.getString("firstname");
          String lstnm = rs.getString("lastname");
          out.println(usn + " " + fstnm + " " + lstnm + " " + "<br />");
      }

      out.print("<h2>Database Details</h2>");
      out.print("Database Product: " + connection.getMetaData().getDatabaseProductName()+"<br/>");
      out.print("Database Driver: " + connection.getMetaData().getDriverName());

      statement.close(); 
      connection.close();
      }
      catch(SQLException e) {
        out.println("SQLException caught: " + e.getMessage());
      }
    %>
    <br />
    <form action="index.jsp">
      <input type="submit" value="Back">
    </form>

  </body>
</html>