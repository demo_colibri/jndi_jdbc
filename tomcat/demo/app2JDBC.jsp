<%@ page import="java.sql.*"%>
<!DOCTYPE html>
<html lang="eng">
<head>
	<title>JDBC/JNDI Database connection</title>
</head>
 
<body>
<h1>App2 connecting with JDBC MySQL database</h1>
<h2>Data from database</h2>
<%
  try {
    // Declarations
    String connectionURL = null;
    String username = null;
    String password = null;
    Connection connection = null;
    Statement statement = null;
    ResultSet rs = null;

    // Initialisations
    Class.forName("com.mysql.jdbc.Driver");
    connectionURL = "jdbc:mysql://db:3306/testdb1";
    username = "appuser1";
    password = "p455W0rD";

    connection = DriverManager.getConnection(connectionURL, username, password);
    statement = connection.createStatement();

    // Database querying; use of data
    String SQLQuery = "SELECT username, firstname, lastname from users";
    rs = statement.executeQuery(SQLQuery);

    out.println("<table><tr><th>Username</th><th>First name</th><th>Last name</th></tr>");
                
    while (rs.next()) {           
      String usn = rs.getString("username");
      String fstnm = rs.getString("firstname");
      String lstnm = rs.getString("lastname");
      out.println("<tr><td>" + usn + "</td><td>" + fstnm + "</td><td>" + lstnm + "</td></tr>");
    }
    out.println("</table>");

    statement.close(); 
    connection.close();
  }
  catch(SQLException e) {
    out.println("SQLException caught: " +e.getMessage());
  }
%>

<br />
<form action="index.jsp">
  <input type="submit" value="Back">
</form>

</body>
</html>