<%@ page import="java.sql.*"%>
<!DOCTYPE html>
<html lang="eng">
<head>
	<title>JDBC/JNDI Database connection</title>
</head>
 
<body>
<h1>App3 connecting with JDBC MySQL database</h1>
<h2>Data from database</h2>
<%
  try {
    // Declarations
    String connectionURL = null;
    String username = null;
    String password = null;
    Connection connection = null;
    Statement statement = null;
    ResultSet rs = null;

    // Initialisations
    Class.forName("com.mysql.jdbc.Driver");
    connectionURL = "jdbc:mysql://db:3306/testdb1";
    username = "appuser1";
    password = "p455W0rD";

    connection = DriverManager.getConnection(connectionURL, username, password);
    statement = connection.createStatement();

    // Database querying; use of data
    String SQLQuery = "SELECT username, firstname, lastname from users";
    rs = statement.executeQuery(SQLQuery);

    String usns = "<th>Username</th>";
    String fstnms = "<th>First name</th>";
    String lstnms = "<th>Last name</th>";

    while (rs.next()) {
      String usn = rs.getString("username");
      String fstnm = rs.getString("firstname");
      String lstnm = rs.getString("lastname");
      usns += "<td>" + usn + "</td>";
      fstnms += "<td>" + fstnm + "</td>";
      lstnms += "<td>" + lstnm + "</td>";
    }

    String output = "<table><tr>" + usns + "</tr><tr>" + fstnms + "</tr><tr>" + lstnms + "</tr></table>";
    out.println(output);

    out.print("<h2>Database Details</h2>");
    out.print("Database Product: " + connection.getMetaData().getDatabaseProductName()+"<br/>");
    out.print("Database Driver: " + connection.getMetaData().getDriverName());

    statement.close(); 
    connection.close();
  }
  catch(SQLException e) {
    out.println("SQLException caught: " +e.getMessage());
  }
%>

<br />

<form action="index.jsp">
  <input type="submit" value="Back">
</form>

</body>
</html>