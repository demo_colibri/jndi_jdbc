<%@ page import="java.sql.*"%>
<DOCTYPE html>
<html>
<head lang="eng">
	<title>JDBC/JNDI Dattabase connection</title>
</head>
 
<body>
<h1>App repository</h1>

<table>
<caption>Applications</caption>
  <tr>
    <th>App</th>
    <th>Characteristics</th>
  </tr>
  <tr>
    <td><form action="app1JDBC.jsp">
      <input type="submit" value="App 1">
    </form>
    </td>
    <td>application 1 jdbc connection</td>
  </tr>
  <tr>
    <td><form action="app2JDBC.jsp">
      <input type="submit" value="App 2">
    </form>
    </td>
    <td>application 2 jdbc connection</td>
  </tr>
  <tr>
    <td><form action="app3JDBC.jsp">
      <input type="submit" value="App 3">
    </form>
    </td>
    <td>application 3 jdbc connection</td>
  </tr>
  <tr>
    <td><form action="app1JNDI.jsp">
      <input type="submit" value="App 1">
    </form>
    </td>
    <td>application 1 jndi connection</td>
  </tr>
  <tr>
    <td><form action="app2JNDI.jsp">
      <input type="submit" value="App 2">
    </form>
    </td>
    <td>application 2 jndi connection</td>
  </tr>
  <tr>
    <td><form action="app3JNDI.jsp">
      <input type="submit" value="App 3">
    </form>
    </td>
    <td>application 3 jndi connection</td>
  </tr>
</body>
</html>