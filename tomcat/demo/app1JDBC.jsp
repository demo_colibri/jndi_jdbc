<%@ page import="java.sql.*"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Demo JDBC/JNDI Database connection</title>
  </head>
  
  <body>
    <h1>App1 connecting with JDBC MySQL database</h1>
    <h2>Data from database</h2>
    <%
      try {
        // Declarations
        String connectionURL = null;
        String username = null;
        String password = null;
        Connection connection = null;
        Statement statement = null;
        ResultSet rs = null;

        // Initialisations
        Class.forName("com.mysql.jdbc.Driver");
        connectionURL = "jdbc:mysql://db:3306/testdb1";
        username = "appuser1";
        password = "p455W0rD";

        connection = DriverManager.getConnection(connectionURL, username, password);
        statement = connection.createStatement();

        // Database querying; use of data
        String SQLQuery = "SELECT username, firstname, lastname from users";
        rs = statement.executeQuery(SQLQuery);

        while (rs.next()) {           
          String usn = rs.getString("username");
          String fstnm = rs.getString("firstname");
          String lstnm = rs.getString("lastname");
          out.println(usn + " " + fstnm + " " + lstnm + " " + "<br />");
      }
      
      out.print("<h2>Database Details</h2>");
      out.print("Database Product: " + connection.getMetaData().getDatabaseProductName()+"<br/>");
      out.print("Database Driver: " + connection.getMetaData().getDriverName());

      statement.close(); 
      connection.close();
      }
      catch(SQLException e) {
        out.println("SQLException caught: " + e.getMessage());
      }
    %>
    <br />
    <form action="index.jsp">
      <input type="submit" value="Back">
    </form>

  </body>
</html>