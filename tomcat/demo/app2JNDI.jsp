<%@ page import="java.sql.*"%>
<%@ page import="javax.naming.Context"%>
<%@ page import="javax.naming.InitialContext"%>
<%@ page import="javax.naming.NamingException"%>
<%@ page import="javax.sql.DataSource"%>
<!DOCTYPE html>
<html lang="eng">
<head>
	<title>JDBC/JNDI Database connection</title>
</head>
 
<body>
<h1>App2 connecting with JNDI MySQL database</h1>
<h2>Data from database</h2>
<%
  try {
    // Declarations
    Context ctx = null;    
    DataSource ds = null;
    Connection connection = null;
    Statement statement = null;
    ResultSet rs = null;

    // Initialisations
    ctx = new InitialContext();
    ds = (DataSource) ctx.lookup("java:/comp/env/jdbc/JndiDBName");

    connection = ds.getConnection();
    statement = connection.createStatement();

    // Database querying; use of data
    String SQLQuery = "SELECT username, firstname, lastname from users";
    rs = statement.executeQuery(SQLQuery);

    out.println("<table><tr><th>Username</th><th>First name</th><th>Last name</th></tr>");
                
    while (rs.next()) {           
      String usn = rs.getString("username");
      String fstnm = rs.getString("firstname");
      String lstnm = rs.getString("lastname");
      out.println("<tr><td>" + usn + "</td><td>" + fstnm + "</td><td>" + lstnm + "</td></tr>");
    }
    out.println("</table>");

    statement.close(); 
    connection.close();
  }
  catch(SQLException e) {
    out.println("SQLException caught: " +e.getMessage());
  }
%>

<br />
<form action="index.jsp">
  <input type="submit" value="Back">
</form>

</body>
</html>