use testdb1;
 
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `firstname` varchar(45) NOT NULL,
  `lastname` varchar(45) NOT NULL,
  `position` varchar(45) NOT NULL,
  `telephone` varchar(45) NOT NULL,
  PRIMARY KEY (`user_id`)
);

INSERT INTO `testdb1`.`users` (`username`, `firstname`, `lastname`, `position`, `telephone`) VALUES ('usn1', 'fstnm1', 'lstnm1', 'pos1', '1234567890');
INSERT INTO `testdb1`.`users` (`username`, `firstname`, `lastname`, `position`, `telephone`) VALUES ('usn2', 'fstnm2', 'lstnm2', 'pos2', '1234567890');
INSERT INTO `testdb1`.`users` (`username`, `firstname`, `lastname`, `position`, `telephone`) VALUES ('usn3', 'fstnm3', 'lstnm3', 'pos3', '1234567890');
INSERT INTO `testdb1`.`users` (`username`, `firstname`, `lastname`, `position`, `telephone`) VALUES ('usn4', 'fstnm4', 'lstnm4', 'pos4', '1234567890');
INSERT INTO `testdb1`.`users` (`username`, `firstname`, `lastname`, `position`, `telephone`) VALUES ('usn5', 'fstnm5', 'lstnm5', 'pos5', '1234567890');

CREATE USER 'appuser1'@'%' IDENTIFIED BY 'p455W0rD';
GRANT SELECT, INSERT ON testdb1.users TO 'appuser1'@'%';
FLUSH PRIVILEGES;
