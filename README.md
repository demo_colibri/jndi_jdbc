# JDBC / JNDI demo

The demonstration relies on a few docker containers.

db implements a MySQL 8 database with the following infos:

MYSQL_DATABASE: testdb1
MYSQL_ROOT_PASSWORD: root@1234
MYSQL_USER: testuser
MYSQL_PASSWORD: user@1234

web implentents a Tomcat webserver where jsp emulates deployed web applications.
Database informations are available in /usr/local/tomcat/conf/context.xml.

This sandbox allows to view potential impacts of database changes.


Prerequisite:
Docker

Start:
docker compose -f docker-compose.yml up --build -d
Navigate to http://localhost:8603/demo

Stop:
docker compose -f docker-compose.yml down
